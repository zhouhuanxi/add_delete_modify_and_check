﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           

            var abc = "select * from student";

            var dt = Dbhelper.GetDataTable(abc);

            dataGridView1.DataSource = dt;
            //单击选中一整行
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //不允许编辑
            dataGridView1.ReadOnly = true;
            //不显示下一个空白行
            dataGridView1.AllowUserToAddRows = false;

        }

        //关闭
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //增加
        private void button3_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
           var res = form2.ShowDialog();
            if (res==DialogResult.Yes)
            {
                var abc = "select * from student";

                var dt = Dbhelper.GetDataTable(abc);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("no");
            }

        }

        //删除
        private void button4_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];

            var cell = row.Cells[0];

            var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;

            var sql = string.Format("delete from student where sId={0},id");
        }

        //修改
        private void button5_Click(object sender, EventArgs e)
        {

            var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells[1].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells[2].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells[3].Value;

            Form2 form2 = new Form2(id,name,age,score);        
            var res = form2.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var abc = "select * from student";

                var dt = Dbhelper.GetDataTable(abc);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("no");
            }

        }

        //查找
        private void button1_Click(object sender, EventArgs e)
        {

            var name = textBox1.Text;

            var sql = string.Format("select * from student where Name like '%{0}%'", name);

            var dt = Dbhelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;

            

        }
    }
}
